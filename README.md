# 🌎 [Planet KDE](https://planet.kde.org)

Planet KDE is a web feed aggregator that collects blog posts from people who contribute to KDE.

If you are a KDE contributor you can have your blog on Planet KDE. Blog content should be mostly
KDE themed and not liable to offend. If you have a general blog you may want to set up a tag and
subscribe the feed for that tag only to Planet KDE.

## Adding your feed
Anyone in the KDE group on invent.kde.org can add new feeds to this repository.

If you want to get your feed added, we prefer Merge Requests via [invent.kde.org](https://invent.kde.org/websites/planet-kde-org).

* Fork this repository
* Edit [planet.ini](https://invent.kde.org/websites/planet-kde-org/tree/master/planet.ini) and add:

```ini
[id]         # replace id with your feed's unique identifier (a-z0-9-_) (eg. kde-dot)
  title    = # title of your feed                                       (eg. KDE Dot)
  feed     = # url to your rss/atom feed                                (eg. https://dot.kde.org/rss.xml)
  link     = # link to the main page of your website                    (eg. https://dot.kde.org)
  location = # two letter language code                                 (eg. en)
  avatar   = # (optional) filename or url of your avatar                (eg. kde.png)
  author   = # (optional) includes various space separated tags about the author:
             #   irc:freenode_nickname     (eg. irc:myircnickname)
             #   gsoc
```

* Upload your avatar to [hackergotchi directory](https://invent.kde.org/websites/planet-kde-org/tree/master/hackergotchi)
* Send a Pull Request

If you do not have a Git account, <a href="https://bugs.kde.org/enter_bug.cgi?product=planet%20kde">file
a bug in Bugzilla</a> listing your name, Git account (if you have one), IRC nick (if you have one), RSS or
Atom feed and what you do in KDE. Attach a photo of your face for hackergotchi.

## Planet KDE Guidelines

Planet KDE is one of the public faces of the KDE project and is read by millions of users and potential
contributors. The content aggregated at Planet KDE is the opinions of its authors, but the sum of that
content gives an impression of the project. Please keep in mind the following guidelines for your blog
content and read the [KDE Code of Conduct](https://www.kde.org/code-of-conduct/). The KDE project
reserves the right to remove an inappropriate blog from the Planet. If that happens multiple times, the
Community Working Group can be asked to consider what needs to happen to get your blog aggregated again.

If you are unsure or have queries about what is appropriate contact the KDE Community Working Group.

### Blogs should be KDE themed

The majority of content in your blog should be about KDE and your work on KDE. Blog posts about personal
subjects are also encouraged since Planet KDE is a chance to learn more about the developers behind KDE.
However blog feeds should not be entirely personal, if in doubt set up a tag for Planet KDE and subscribe
the feed from that tag so you can control what gets posted.

### Posts should be constructive

Posts can be positive and promote KDE, they can be constructive and lay out issues which need to be
addressed, but blog feeds should not contain useless, destructive and negative material. Constructive
criticism is welcome and the occasional rant is understandable, but a feed where every post is critical
and negative is unsuitable. This helps to keep KDE overall a happy project.

### You must be a KDE contributor

Only have your blog on Planet KDE if you actively contribute to KDE, for example through code, user
support, documentation etc.

### It must be a personal blog, or in a blog class

Planet KDE is a collection of blogs from KDE contributors.

### Do not inflame

KDE covers a wide variety of people and cultures. Profanities, prejudice, lewd comments and content
likely to offend are to be avoided. Do not make personal attacks or attacks against other projects on
your blog.

For further guidance on good practice see the <a href="https://www.kde.org/code-of-conduct/">KDE Code of Conduct</a>.

## Development environment
To run this website locally, use the following commands:
```sh
bundler install
bundler exec rake build
bundler exec jekyll serve
```
and visit [127.0.0.1:4000](http://127.0.0.1:4000)

